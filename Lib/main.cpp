#include "stm8s.h"
#include "math.h"

volatile uint16_t iteration = 0;
volatile uint16_t valuePulse = 0;
volatile bool newPulseCompleted = FALSE;
volatile bool needCalculateNewPulseTime = FALSE;

#define MIN_PULSE 24
#define MAX_PULSE 275
#define PULSE_DEAD_TIME 8
#define MIN_VOLTAGE 55
#define MAX_VOLTAGE 275
#define PULSE_TABLE_SIZE 72
#define VOLTAGE_TABLE_SIZE 20
#define OPT2 0x4803
#define ALTERNATE_FUNC_TIM1 0x20
#define NOPT2 0x4804
#define ALTERNATE_FUNC_TIM1 0x20

float sin_table[PULSE_TABLE_SIZE] = 
{
  0,
  0.087155743,
  0.173648178,
  0.258819045,
  0.342020143,
  0.422618262,
  0.5,
  0.573576436,
  0.64278761,
  0.707106781,
  0.766044443,
  0.819152044,
  0.866025404,
  0.906307787,
  0.939692621,
  0.965925826,
  0.984807753,
  0.996194698,
  1,
  0.996194698,
  0.984807753,
  0.965925826,
  0.939692621,
  0.906307787,
  0.866025404,
  0.819152044,
  0.766044443,
  0.707106781,
  0.64278761,
  0.573576436,
  0.5,
  0.422618262,
  0.342020143,
  0.258819045,
  0.173648178,
  0.087155743,
  1.22515E-16,
  -0.087155743,
  -0.173648178,
  -0.258819045,
  -0.342020143,
  -0.422618262,
  -0.5,
  -0.573576436,
  -0.64278761,
  -0.707106781,
  -0.766044443,
  -0.819152044,
  -0.866025404,
  -0.906307787,
  -0.939692621,
  -0.965925826,
  -0.984807753,
  -0.996194698,
  -1,
  -0.996194698,
  -0.984807753,
  -0.965925826,
  -0.939692621,
  -0.906307787,
  -0.866025404,
  -0.819152044,
  -0.766044443,
  -0.707106781,
  -0.64278761,
  -0.573576436,
  -0.5,
  -0.422618262,
  -0.342020143,
  -0.258819045,
  -0.173648178,
  -0.087155743
};

volatile static uint8_t workTableIndex;
volatile static uint8_t calculateTableIndex;
volatile static uint16_t pulseTimes[2][PULSE_TABLE_SIZE];
volatile static uint16_t voltageBuffer[VOLTAGE_TABLE_SIZE];

void Timer1Init(void)
{
  CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER1, ENABLE);
  TIM1_TimeBaseInit(0, TIM1_COUNTERMODE_UP, 550, 0);   //Tick = 62,5 ns; Period = 550 * 62,5 ns = 34,375 us; Freq = 29090,90 Hz
  TIM1_OC2Init(TIM1_OCMODE_PWM1, TIM1_OUTPUTSTATE_ENABLE, TIM1_OUTPUTNSTATE_ENABLE,
               MIN_PULSE, TIM1_OCPOLARITY_HIGH, TIM1_OCNPOLARITY_HIGH, TIM1_OCIDLESTATE_SET,
               TIM1_OCNIDLESTATE_SET);
  TIM1_BDTRConfig(TIM1_OSSISTATE_ENABLE, TIM1_LOCKLEVEL_OFF, PULSE_DEAD_TIME, TIM1_BREAK_DISABLE,
                  TIM1_BREAKPOLARITY_HIGH, TIM1_AUTOMATICOUTPUT_ENABLE);
  TIM1_ITConfig(TIM1_IT_UPDATE, ENABLE);
  TIM1_ClearITPendingBit(TIM1_IT_UPDATE);
}

void ADC1Init(void)
{
  CLK_PeripheralClockConfig(CLK_PERIPHERAL_ADC, ENABLE);
  GPIO_Init(GPIOB, GPIO_PIN_7, GPIO_MODE_IN_FL_NO_IT);
  ADC1_DeInit();
  ADC1_Init(ADC1_CONVERSIONMODE_CONTINUOUS, ADC1_CHANNEL_7, ADC1_PRESSEL_FCPU_D2,
            ADC1_EXTTRIG_TIM, DISABLE, ADC1_ALIGN_RIGHT, ADC1_SCHMITTTRIG_CHANNEL7,
            DISABLE);
}

void Calculate()
{
  uint32_t voltage = 0;
  for(int i = 0; i < VOLTAGE_TABLE_SIZE; ++i)
  {
    voltage += voltageBuffer[i];
  }
  voltage = voltage / VOLTAGE_TABLE_SIZE;
  
  valuePulse = MAX_VOLTAGE - voltage * (MAX_VOLTAGE - MIN_VOLTAGE) / 0x03FF;
  
  float pulseTemp;
  for(int i = 0; i < PULSE_TABLE_SIZE; ++i)
  {
    pulseTemp = sin_table[i]*(valuePulse - MIN_PULSE) + MAX_PULSE;
    pulseTimes[calculateTableIndex][i] = pulseTemp;
  }
}

void InitBuffer(void)
{
  workTableIndex = 0;
  calculateTableIndex = 1;
  memset(pulseTimes[workTableIndex], 0, PULSE_TABLE_SIZE);
  memset(pulseTimes[calculateTableIndex], 0, PULSE_TABLE_SIZE);
  memset(voltageBuffer, 0, VOLTAGE_TABLE_SIZE);
  Calculate();
  memcpy(pulseTimes[workTableIndex], pulseTimes[calculateTableIndex], PULSE_TABLE_SIZE);
}

void AddNewVoltage(uint16_t newVoltage)
{
  uint16_t voltageTemp;
  for(int i = VOLTAGE_TABLE_SIZE - 1; i > 0; --i)
  {
    voltageBuffer[i] = voltageBuffer[i - 1];
  }
  voltageBuffer[0] = newVoltage;
}

int main()
{
  CLK_HSECmd(ENABLE);
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);
  FLASH_DeInit();  
  FLASH_Unlock(FLASH_MEMTYPE_DATA);
  ADC1Init();
  if(FLASH_ReadOptionByte(OPT2) != 0x20DF)
  {
    FLASH_ProgramOptionByte(OPT2, 0x20);
  }
  
  Timer1Init();
  
  enableInterrupts();
  
  ADC1_Cmd(ENABLE);
  
  GPIO_Init(GPIOE, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST);

  InitBuffer();
  TIM1_Cmd(ENABLE);
  TIM1_ARRPreloadConfig(ENABLE);
  TIM1_CtrlPWMOutputs(ENABLE);
  
  while(TRUE)
  {
    if(needCalculateNewPulseTime == TRUE)
    {
      newPulseCompleted = FALSE;
      AddNewVoltage((ADC1->DRH << 8) | ADC1->DRL);
      Calculate();
      needCalculateNewPulseTime = FALSE;
      newPulseCompleted = TRUE;
      ADC1_StartConversion();
    }
  }
}

uint16_t iterDelay =0;

INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
{
  TIM1_SetCompare2(pulseTimes[workTableIndex][iteration]);
  
  if(iteration == 18)
  {
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
  }
  else if(iteration == 54)
  {
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
  }
  iteration++;
  
  if(iteration >= PULSE_TABLE_SIZE)
  {
    iteration = 0;
    if(newPulseCompleted == TRUE)
    {
      uint8_t tempIndex = calculateTableIndex;
      calculateTableIndex = workTableIndex;
      workTableIndex = tempIndex;
      newPulseCompleted = FALSE;
    }
    needCalculateNewPulseTime = TRUE;
  }

  TIM1_ClearITPendingBit(TIM1_IT_UPDATE);
}
